# Revolve

Optimal checkpointing code by Andreas Griewan, Andrea Walther and Philipp Stumm. Some small modification to fit our need. The upstream URL is https://math.uni-paderborn.de/fileadmin/mathematik/AG-Mathematik-u-i-Anwendungen/Software/revolve.tar.